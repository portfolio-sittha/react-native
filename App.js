/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import React from 'react';

import {createStackNavigator} from '@react-navigation/stack';

import RegisterScreen from './register';
import MainScreen from './main';
import ShowRegisterScreen from './show-register';
import {applyMiddleware, combineReducers, createStore} from 'redux';
import ProfileRedurec from './redux/profile-redux';
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';

const Stack = createStackNavigator();
const StoreReducers = combineReducers({
  profile: ProfileRedurec,
});
const store = createStore(StoreReducers, applyMiddleware(thunk));
function App() {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Home" component={RegisterScreen} />
          <Stack.Screen
            name="Main"
            options={{headerLeft: null}}
            component={MainScreen}
          />
          <Stack.Screen name="Show" component={ShowRegisterScreen} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}

export default App;
