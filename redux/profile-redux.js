const profileModel = {
  name: null,
  mobile: null,
};

export default function ProfileRedurec(state = profileModel, action) {
  switch (action.type) {
    case 'SET_PROFILE':
      return {state, ...action.payload};
    case 'CLEAR_PROFILE':
      return {state, ...action.payload};
    default:
      return state;
  }
}

export class ProfileDispatcher {
  dispatch;

  constructor(dispatch) {
    this.dispatch = dispatch;
  }
  update = (value) => {
    return this.dispatch({
      type: 'SET_PROFILE',
      payload: value,
    });
  };

  reset = () => {
    return this.dispatch({
      type: 'CLEAR_PROFILE',
      payload: undefined,
    });
  };
}
